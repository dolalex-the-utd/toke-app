;(function() {

  $('.currency-picker').select2();

  // TOGGLE HAMBURGER & COLLAPSE NAV
  $('.nav-toggle').on('click', function() {
    $('.menu--main').toggleClass('open');
  });

  $(document).on('click touchstart', function (e) {
    if (e.target.closest('.menu--main') || e.target.closest('.select2-container')) {
      return;
    }
    $('.menu--main').removeClass('open');
  });



// REMOVE X & COLLAPSE NAV ON ON CLICK
  $('.menu__list a').on('click', function() {
    $('.menu--main').removeClass('open');
  });

  $(window).resize(function () {
    if ($(window).width() >= 991) {
      $('.menu--main').removeClass('open');
    }
  });

}());