;(function() {

  var modal = document.getElementById('subscribe-popup');
  var span = document.getElementsByClassName('modal__close')[0];

  if (window.location.href.indexOf('?customer_posted=true') > -1) {
    modal.style.display = 'block';
  }

  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = 'none';
      history.pushState(null, '', '/')
    }
  };

  span.onclick = function() {
    modal.style.display = "none";
    history.pushState(null, '', '/')
  }

}());