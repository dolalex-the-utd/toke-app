;(function() {

  $(document).ready(function () {

    initInstafeed();

    function initInstafeed() {
      var instafeeds = $('.instafeed__box');

      instafeeds.each(function(index, el){
        var id = $(el).attr('id');

        var instafeed = $('#' + id);

        if (instafeed.length) {
          var token = instafeed.data('token');
          var userId = instafeed.data('userid');

          if (!token || !userId) {
            return;
          }

          var feed = new Instafeed({
              target: id,
              get: 'user',
              userId: userId,
              resolution: 'standard_resolution',
              accessToken: token,
              limit: 10,
              template: '<div class="instagram__item"><div class="instagram__picture"><a class="instagram__image" href="{{link}}" target="_blank" style="background-image:url({{image}})"></a></div></div>',
              after: function () {
                if (id == 'instafeed-2') {
                  createInstaSlick()
                }
              }
            }
          );
          feed.run();
        }
      })
    }

    function createInstaSlick() {

      $('#instafeed-2').slick({
        dots: false,
        arrows: true,
        slidesToShow: 4,
        responsive: [
          {
            breakpoint: 1231,
            settings: {
              slidesToShow: 4
            }
          },
          {
            breakpoint: 920,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 568,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });
    }


  });
}());