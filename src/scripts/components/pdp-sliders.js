;(function() {

  let sliderMini = $('.product-slider_mini');

  let prevSlide = $('.js-slider-actions .slick-prev');
  let nextSlide = $('.js-slider-actions .slick-next');

  sliderMini.on('init reInit', function(event, slick){
    if (slick.slideCount < 5) {
      $('.js-slider-actions').addClass('not-active');
    }
  });


  $('.product-slider-main').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.product-slider_mini'
  });

  sliderMini.slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.product-slider-main',
    focusOnSelect: true,
    dots: false,
    arrows: false,
    centerPadding: '5px',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });

  prevSlide.click(function () {
    sliderMini.slick('slickPrev');
  });

  nextSlide.click(function () {
    sliderMini.slick('slickNext');
  });

}());